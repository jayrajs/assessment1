//Load library
var express = require("express");
var path = require("path");

//Create an instance of express app
var app = express();

var allUsers = [];


var createUser = function(abc) {
    return ({
			email: abc.email,
		 password: abc.password,
		    name:  abc.name,
		   gender: abc.gender,
	  DateOfBirth: abc.dateofbirth,
		  address: abc.address,
		  country: abc.country,
		  contact: abc.contact
    });
}


// config the port
app.set("port", parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);



app.get("/register", function(req, resp) {

    allUsers.push(createUser(req.query));
    resp.status(201).end();

});


app.get("/register-pending", function(req, resp) {
    resp.status(200);
    resp.type("application/json");
    resp.json(allUsers);
});


//Define routes, load the static resources from the following directory
app.use("/libs", express.static(path.join(__dirname, "bower_components"))); //setting the path for /libs folder
app.use(express.static(path.join(__dirname, "/public")));
app.use(express.static(path.join(__dirname, "/image")));


// Start the server
app.listen(app.get("port"), function(){
    console.log("Application started at port --> %d" , app.get("port"));
    console.log("Application started on --> %s" , new Date());
});