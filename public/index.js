//Place order
(function() {
	var TableApp = angular.module("TableApp", []);

	var initForm = function(abc) {
		abc.email = "";
		abc.password = "";
		abc.name = "";
		abc.gender = "";
		abc.dateOfBirth = "";
		abc.address = "";
		abc.country = "";
		abc.contact = "";
	}

	var createQueryString = function(abc) {
		return ({
			email: abc.email,
		 password: abc.password,
		    name:  abc.name,
		   gender: abc.gender,
	  DateOfBirth: abc.dateOfBirth,
		  address: abc.address,
		  country: abc.country,
		  contact: abc.contact
		});
	}

	var TableCtrl = function($http) {
		var tableCtrl = this;

		initForm(tableCtrl);

		tableCtrl.placeOrder = function() {
			$http.get("/register", {
			params: createQueryString(tableCtrl)
			});
			initForm(tableCtrl);
			javascript:alert( 'Thank you for submitting!' );
		}

	};
	

	TableApp.controller("TableCtrl", [ "$http",  TableCtrl ]);
})();